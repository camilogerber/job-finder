package com.example.jobfinder.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EndpointUrl {
    SIGN_UP("http://www.mocky.io/v2/");

    private final String url;
}
