package com.example.jobfinder.activities.step3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.example.jobfinder.R;

public class BudgetSentSuccessActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_budget_sent_success);

        Button back = findViewById(R.id.back_to_user_profile);

        back.setOnClickListener(v -> finish());
    }
}
