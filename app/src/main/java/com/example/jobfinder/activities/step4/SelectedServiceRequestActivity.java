package com.example.jobfinder.activities.step4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jobfinder.R;
import com.example.jobfinder.services.SessionManager;

public class SelectedServiceRequestActivity extends AppCompatActivity {

    SessionManager sessionManager = SessionManager.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_service_request);

        int serviceRequestIndex = getIntent().getIntExtra("serviceIndex",0);

        TextView name = findViewById(R.id.selected_service_name);
        TextView surname = findViewById(R.id.selected_service_surname);

        name.setText(sessionManager.getElement(serviceRequestIndex).getUser().getName());
        surname.setText(sessionManager.getElement(serviceRequestIndex).getUser().getSurname());

        Button confirmService = findViewById(R.id.confirm_service);
        Button cancelService = findViewById(R.id.cancel_service);

        cancelService.setOnClickListener(v -> deleteOrder(serviceRequestIndex));

        confirmService.setOnClickListener(v -> {
            sessionManager.getServiceRequestList().get(serviceRequestIndex).setCompleted(true);
            startActivity(new Intent(
                    getApplicationContext(),
                    ConfirmedServiceActivity.class));
            finish();
        });
    }

    private void deleteOrder(int index) {
        sessionManager.deleteRequest(index);
        startActivity(new Intent(getApplicationContext(),MyContractsActivity.class));
        Toast.makeText(
                getApplicationContext(),
                "Solicitud de servicio eliminada!",
                Toast.LENGTH_SHORT).show();
        finish();
    }
}
