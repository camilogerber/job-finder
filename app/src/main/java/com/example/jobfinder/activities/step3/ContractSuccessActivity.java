package com.example.jobfinder.activities.step3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.example.jobfinder.R;
import com.example.jobfinder.activities.step4.MyContractsActivity;

public class ContractSuccessActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contract_success);

        Button back = findViewById(R.id.back_to_user_from_success_contract);

        back.setOnClickListener(v -> {
            startActivity(new Intent(
                    getApplicationContext(),
                    MyContractsActivity.class));
            finish();
        });
    }
}
