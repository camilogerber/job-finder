package com.example.jobfinder.activities.step2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import com.example.jobfinder.R;
import com.example.jobfinder.activities.step3.SelectedCategoryActivity;
import com.example.jobfinder.adapters.CategoryAdapter;
import com.example.jobfinder.services.SessionManager;

public class CategoryActivity extends AppCompatActivity {

    private SessionManager sessionManager = SessionManager.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        ListView categoryList = findViewById(R.id.categories_listview);

        CategoryAdapter adapter = new CategoryAdapter(
                getApplicationContext(),
                R.layout.category_item,
                sessionManager.getCategories());

        categoryList.setAdapter(adapter);

        categoryList.setOnItemClickListener((adapterView, view, position, l) -> {
            Intent intent = new Intent(
                    getApplicationContext(),
                    SelectedCategoryActivity.class);
            intent.putExtra("categoryIndex", position);
            startActivity(intent);
        });
    }
}
