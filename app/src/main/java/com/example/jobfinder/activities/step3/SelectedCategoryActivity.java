package com.example.jobfinder.activities.step3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import com.example.jobfinder.R;
import com.example.jobfinder.adapters.UserItemAdapter;
import com.example.jobfinder.services.SessionManager;

public class SelectedCategoryActivity extends AppCompatActivity {

    private SessionManager sessionManager = SessionManager.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_category);

        int categoryIndex = getIntent().getIntExtra("categoryIndex",0);

        ListView userList = findViewById(R.id.users_listview);

        UserItemAdapter userItemAdapter = new UserItemAdapter(
                getApplicationContext(),
                R.layout.user_item,
                sessionManager.getUsers());
        userList.setAdapter(userItemAdapter);

        userList.setOnItemClickListener((adapterView, view, position, l) -> {
            Intent intent = new Intent(
                    getApplicationContext(),
                    SelectedUserActivity.class);
            intent.putExtra("userIndex", position);
            intent.putExtra("categoryIndex",categoryIndex);
            startActivity(intent);
        });

    }
}
