package com.example.jobfinder.activities.step1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.jobfinder.activities.step2.MainMenuActivity;
import com.example.jobfinder.R;

import java.util.Objects;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Button regButton = findViewById(R.id.login_sign_up_button);
        Button loginButton = findViewById(R.id.login_button);
        EditText email = findViewById(R.id.login_email);
        EditText password = findViewById(R.id.login_password);

        loginButton.setOnClickListener(v -> checkFields(
                Objects.requireNonNull(email.getText().toString()),
                Objects.requireNonNull(password.getText().toString())
        ));

        regButton.setOnClickListener(v -> startSignUpActivity());
    }

    private void checkFields(String email, String password) {
        if(email.isEmpty() || password.isEmpty()){
            Toast.makeText(getApplicationContext(),
                    "Complete todos los campos",
                    Toast.LENGTH_SHORT).show();
        } else {
            performLogin(email,password);
        }
    }

    private void performLogin(String email, String password) {
        startActivity(new Intent(getApplicationContext(), MainMenuActivity.class));
        finish();
        //Llamo al servicio de login
    }

    private void startSignUpActivity() {
        Intent next = new Intent(getApplicationContext(),SignUpActivity.class);
        startActivity(next);
    }
}
