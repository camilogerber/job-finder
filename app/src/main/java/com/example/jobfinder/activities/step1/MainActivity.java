package com.example.jobfinder.activities.step1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.jobfinder.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }
}
