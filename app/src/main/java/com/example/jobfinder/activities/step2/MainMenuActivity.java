package com.example.jobfinder.activities.step2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.example.jobfinder.R;
import com.example.jobfinder.activities.step4.MyContractsActivity;

public class MainMenuActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu2);
        Button categories = findViewById(R.id.category_button);
        Button serviceRequestButton = findViewById(R.id.my_requests_button);

        serviceRequestButton.setOnClickListener(v-> startActivity(new Intent(
                getApplicationContext(),
                MyContractsActivity.class)));

        categories.setOnClickListener(v -> startActivity(
                new Intent(getApplicationContext(),
                        CategoryActivity.class)));
    }
}
