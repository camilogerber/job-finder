package com.example.jobfinder.activities.step4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.jobfinder.R;
import com.example.jobfinder.adapters.ServiceRequestAdapter;
import com.example.jobfinder.services.SessionManager;

public class MyContractsActivity extends AppCompatActivity {

    private SessionManager sessionManager = SessionManager.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_contracts);

        ListView contracts = findViewById(R.id.my_contracts_listview);

        TextView noContractsMessage = findViewById(R.id.no_contracts);

        if (sessionManager.getServiceRequestList().isEmpty()){
            contracts.setVisibility(View.GONE);
            noContractsMessage.setVisibility(View.VISIBLE);
        }

        ServiceRequestAdapter adapter = new ServiceRequestAdapter(
                getApplicationContext(),
                R.layout.service_request_item,
                sessionManager.getServiceRequestList());

        contracts.setAdapter(adapter);

        contracts.setOnItemClickListener((adapterView, view, position, l) -> {
            Intent intent = new Intent(
                    getApplicationContext(),
                    SelectedServiceRequestActivity.class);
            intent.putExtra("serviceIndex", position);
            startActivity(intent);
            finish();
        });
    }
}
