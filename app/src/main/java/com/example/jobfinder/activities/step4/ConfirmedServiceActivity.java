package com.example.jobfinder.activities.step4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.example.jobfinder.R;
import com.example.jobfinder.activities.step2.MainMenuActivity;

public class ConfirmedServiceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmed_service);

        Button back = findViewById(R.id.back_to_main);

        back.setOnClickListener(v -> {
            startActivity(new Intent(
                    getApplicationContext(),
                    MainMenuActivity.class)
            );

            finish();
        });
    }
}
