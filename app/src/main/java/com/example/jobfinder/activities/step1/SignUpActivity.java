package com.example.jobfinder.activities.step1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.jobfinder.R;
import com.example.jobfinder.entities.User;
import com.example.jobfinder.repository.UsersRepository;
import com.example.jobfinder.services.ApiUtils;

import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.jobfinder.enums.EndpointUrl.SIGN_UP;

public class SignUpActivity extends AppCompatActivity {

    private UsersRepository usersRepository = ApiUtils.getAPIService(SIGN_UP.getUrl());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        final EditText email = findViewById(R.id.reg_email);
        final EditText name = findViewById(R.id.reg_name);
        final EditText surname = findViewById(R.id.reg_surname);
        final EditText password = findViewById(R.id.reg_password);
        final EditText dni = findViewById(R.id.reg_dni);
        final EditText confPass = findViewById(R.id.reg_confirm_password);
        final Button regButton = findViewById(R.id.sign_up_button);

        regButton.setOnClickListener(v -> checkFields(
                Objects.requireNonNull(email.getText()).toString(),
                Objects.requireNonNull(name.getText()).toString(),
                Objects.requireNonNull(surname.getText()).toString(),
                Objects.requireNonNull(password.getText()).toString(),
                Objects.requireNonNull(confPass.getText()).toString(),
                Objects.requireNonNull(dni.getText()).toString())
        );
    }

    private void checkFields(String email, String name, String surname, String password, String confPass, String dni) {
        if(email.isEmpty() || name.isEmpty() || surname.isEmpty() || password.isEmpty() || confPass.isEmpty()){
            Toast.makeText(getApplicationContext(), "Complete todos los campos", Toast.LENGTH_SHORT).show();
        } else {
            Integer.valueOf(dni);
            if(!password.equals(confPass)){
                Toast.makeText(getApplicationContext(), "Las contraseñas no coinciden", Toast.LENGTH_SHORT).show();
            } else {
                performSignUp(email,name,surname,password,dni);
            }
        }
    }

    private void performSignUp(String email, String name, String surname, String password, String dni) {
        User newUser = User.builder()
                .email(email)
                .name(name)
                .surname(surname)
                .password(password)
                .dni(dni)
                .build();

        //Llamo al servicio de registro
        usersRepository.signUpUser(newUser).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.i("[RESPONSE]",response.toString());
                Log.i("[CODE]",response.code() + "");
                if(response.code() == 200){
                    startActivity(new Intent(getApplicationContext(),SignUpSuccessActivity.class));
                    finish();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("[ERROR]",t.toString());
                Toast.makeText(
                        getApplicationContext(),
                        "Hubo un conflicto al registrar el usuario",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
}
