package com.example.jobfinder.activities.step3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.example.jobfinder.R;

public class BudgetActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_budget);

        Button sendButton = findViewById(R.id.send_request_comment);

        sendButton.setOnClickListener(v-> sentMenssage());
    }

    private void sentMenssage() {
        startActivity(new Intent(
                getApplicationContext(),
                BudgetSentSuccessActivity.class)
        );
        finish();
    }
}
