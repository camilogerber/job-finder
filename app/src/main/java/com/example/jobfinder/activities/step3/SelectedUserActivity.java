package com.example.jobfinder.activities.step3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.example.jobfinder.R;
import com.example.jobfinder.entities.ServiceRequest;
import com.example.jobfinder.services.SessionManager;

public class SelectedUserActivity extends AppCompatActivity {

    private SessionManager sessionManager = SessionManager.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_user);

        Button budgetButton = findViewById(R.id.budget_button);
        Button confirmUser = findViewById(R.id.confirm_user);

        int userId = getIntent().getIntExtra("userIndex",0);
        int catId = getIntent().getIntExtra("categoryIndex",0);

        TextView name = findViewById(R.id.selected_user_name);
        TextView surname = findViewById(R.id.selected_user_surname);

        confirmUser.setOnClickListener(v -> confirmOrder(userId,catId));

        name.setText(sessionManager.getUsers()[userId].getName());
        surname.setText(sessionManager.getUsers()[userId].getSurname());

        budgetButton.setOnClickListener(v -> startActivity(new Intent(
                getApplicationContext(),
                BudgetActivity.class))
        );

    }

    private void confirmOrder(int userId, int catId) {
        ServiceRequest serviceRequest = ServiceRequest.builder()
                .category(sessionManager.getCategories()[catId])
                .user(sessionManager.getUsers()[userId])
                .id(userId)
                .isCompleted(false)
                .build();
        sessionManager.addRequest(serviceRequest);

        startActivity(new Intent(getApplicationContext(), ContractSuccessActivity.class));
        finish();
    }
}
