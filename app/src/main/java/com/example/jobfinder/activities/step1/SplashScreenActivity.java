package com.example.jobfinder.activities.step1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.jobfinder.R;

import java.util.concurrent.TimeUnit;

/** Message from developer:
 *
 *
 * Don't steal my code, thief.
 */
public class SplashScreenActivity extends AppCompatActivity {

    private final long SPLASH_DISPLAY_LENGTH = TimeUnit.SECONDS.toMillis(2);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        new Handler().postDelayed(() -> {
            Intent mainIntent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(mainIntent);
            finish();
        }, SPLASH_DISPLAY_LENGTH);
    }
}
