package com.example.jobfinder.activities.step1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.example.jobfinder.R;

public class SignUpSuccessActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_success);
        Button returnButton = findViewById(R.id.back_to_login);

        returnButton.setOnClickListener(v -> {
            startActivity(
                new Intent(
                        getApplicationContext(),
                        LoginActivity.class));
        finish();
        });
    }
}
