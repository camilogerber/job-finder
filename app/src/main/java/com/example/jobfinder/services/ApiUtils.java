package com.example.jobfinder.services;

import com.example.jobfinder.repository.UsersRepository;

public class ApiUtils {

    private ApiUtils() {}

    public static UsersRepository getAPIService(String url) {

        return RetrofitClient.getClient(url).create(UsersRepository.class);
    }
}
