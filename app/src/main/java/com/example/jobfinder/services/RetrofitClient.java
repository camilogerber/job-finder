package com.example.jobfinder.services;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static Retrofit retrofit = null;
    private static String lastValidUrl = null;

    public static Retrofit getClient(String baseUrl) {
        if (retrofit==null || !lastValidUrl.equals(baseUrl)) {
            lastValidUrl = baseUrl;
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
