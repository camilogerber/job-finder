package com.example.jobfinder.services;

import com.example.jobfinder.entities.Category;
import com.example.jobfinder.entities.ServiceRequest;
import com.example.jobfinder.entities.UserResponse;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SessionManager {

    private static SessionManager sessionManager = null;

    private final Category[] categories = {
            new Category(1,"Construcción"),
            new Category(2,"Limpieza"),
            new Category(3,"Informática"),
            new Category(4,"Reparaciones"),
            new Category(5,"Zapatería"),
            new Category(6,"Carpintería")
    };

    private List<ServiceRequest> serviceRequestList = new ArrayList<>();

    private final UserResponse[] users = {
            new UserResponse(1,"Rodrigo","Tapari","rodrigotapari@gmail.com",12345678,"123456"),
            new UserResponse(2,"Julian","Serrano","julianserrano@gmail.com",123455,"1232342"),
            new UserResponse(3,"Pedro","Dominguez","pedrodominguez@gmail.com",1232423,"adsfsdfds"),
            new UserResponse(4, "Julio", "Nuñez","julionuñez@gmail.com",4234234,"afsdfsd")
    };

    private SessionManager(){}

    public static SessionManager getInstance(){
        if (sessionManager == null) sessionManager = new SessionManager();
        return sessionManager;
    }

    public void addRequest(ServiceRequest serviceRequest){
        serviceRequestList.add(serviceRequest);
    }

    public ServiceRequest getElement(int index){
        return serviceRequestList.get(index);
    }

    public void deleteRequest(int index){
        serviceRequestList.remove(index);
    }
}
