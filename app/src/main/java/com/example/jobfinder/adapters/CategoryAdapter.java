package com.example.jobfinder.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.jobfinder.R;
import com.example.jobfinder.entities.Category;

public class CategoryAdapter extends ArrayAdapter<Category> {

    private int resourceLayout;
    private Context mContext;

    public CategoryAdapter(Context context, int resource, Category[] items) {
        super(context, resource, items);
        this.resourceLayout = resource;
        this.mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(mContext);
            v = vi.inflate(resourceLayout, null);
        }

        Category p = getItem(position);

        if (p != null) {
            TextView tt1 = v.findViewById(R.id.list_category_textview);

            if (tt1 != null) {
                tt1.setText(p.getName());
            }
        }

        return v;
    }
}
