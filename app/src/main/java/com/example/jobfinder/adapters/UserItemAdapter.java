package com.example.jobfinder.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.jobfinder.R;
import com.example.jobfinder.entities.UserResponse;

public class UserItemAdapter extends ArrayAdapter<UserResponse> {

    private int resourceLayout;
    private Context mContext;

    public UserItemAdapter(Context context, int resource, UserResponse[] items) {
        super(context, resource, items);
        this.resourceLayout = resource;
        this.mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(mContext);
            v = vi.inflate(resourceLayout, null);
        }

        UserResponse p = getItem(position);

        if (p != null) {
            TextView tt1 = v.findViewById(R.id.user_name);
            TextView tt2 = v.findViewById(R.id.user_surname);

            if (tt1 != null) {
                tt1.setText(p.getName());
            }

            if (tt2 != null){
                tt2.setText(p.getSurname());
            }
        }

        return v;
    }
}
