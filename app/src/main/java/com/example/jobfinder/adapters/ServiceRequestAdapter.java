package com.example.jobfinder.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.jobfinder.R;
import com.example.jobfinder.entities.ServiceRequest;

import java.util.List;

public class ServiceRequestAdapter extends ArrayAdapter<ServiceRequest> {

    private int resourceLayout;
    private Context mContext;

    public ServiceRequestAdapter(Context context, int resource, List<ServiceRequest> items) {
        super(context, resource, items);
        this.resourceLayout = resource;
        this.mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(mContext);
            v = vi.inflate(resourceLayout, null);
        }

        ServiceRequest p = getItem(position);

        if (p != null) {
            TextView name = v.findViewById(R.id.service_request_name);
            TextView surname = v.findViewById(R.id.service_request_surname);
            TextView state = v.findViewById(R.id.service_request_state);

            if (name != null) {
                name.setText(p.getUser().getName());
            }
            if (surname != null) {
                surname.setText(p.getUser().getSurname());
            }
            if (state != null) {
                if (p.isCompleted()){
                    state.setText("Realizado");
                    state.setTextColor(Color.GREEN);
                } else{
                    state.setText("Pendiente");
                    state.setTextColor(Color.RED);
                }

            }
        }

        return v;
    }
}
