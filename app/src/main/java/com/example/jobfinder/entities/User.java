package com.example.jobfinder.entities;

import com.google.gson.annotations.SerializedName;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class User {
    private String email;
    private String name;
    private String surname;
    private String password;
    private String dni;
}
