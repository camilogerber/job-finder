package com.example.jobfinder.entities;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ServiceRequest {

    private Integer id;
    private UserResponse user;
    private Category category;
    private boolean isCompleted;
}
