package com.example.jobfinder.entities;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserResponse {
    private Integer id;
    private String name;
    private String surname;
    private String email;
    private Integer dni;
    private String password;
}
