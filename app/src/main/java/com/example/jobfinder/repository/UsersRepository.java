package com.example.jobfinder.repository;

import com.example.jobfinder.entities.User;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UsersRepository {

    @POST("5eadfc512f000050001988d0")
    Call<ResponseBody> signUpUser(@Body User user);
}
